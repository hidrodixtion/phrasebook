package com.projectbox.phrasebook;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by hidrodixtion on 6/24/15.
 */
public class SettingsActivity extends BaseActivity {
	@Bind(R.id.app_bar)
	Toolbar toolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		ButterKnife.bind(this);

		setSupportActionBar(toolbar);

		ActionBar appBar = getSupportActionBar();
		if (appBar != null) {
			appBar.setTitle("Settings");
			appBar.setHomeButtonEnabled(true);
			appBar.setDisplayHomeAsUpEnabled(true);
		}

		getFragmentManager().beginTransaction().replace(R.id.content, new PrefsFragment()).commit();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == android.R.id.home) {
			Intent i = new Intent();

			setResult(MainActivity.RES_SETTING, i);
			finish();
//			NavUtils.navigateUpFromSameTask(this);
		}

		return true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ButterKnife.unbind(this);
	}

	public static class PrefsFragment extends PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_general);
		}
	}
}
