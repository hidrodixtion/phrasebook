package com.projectbox.phrasebook;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.projectbox.phrasebook.adapter.SearchTabPagerAdapter;
import com.projectbox.phrasebook.data.QueryHelper;
import com.projectbox.phrasebook.data.RawParser;
import com.projectbox.phrasebook.event.FavClickEvent;
import com.projectbox.phrasebook.stats.GoogleAnalytics;
import com.projectbox.phrasebook.utility.SoundPlayer;

import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

public class SearchActivity extends BaseActivity {

	@Bind(R.id.toolbar)
	Toolbar toolbar;
	@Bind(R.id.view_pager_search)
	ViewPager viewPager;
	@Bind(R.id.app_tab)
	TabLayout tabLayout;
	@Bind(R.id.activity_search)
	LinearLayout activitySearch;

	protected SearchActivityFragment mFragment;
	protected QueryHelper mQueryHelper;
	protected RawParser mParser;
	protected String mSearch = "";
	SearchView searchView;
	protected final String SEARCH_KEY = "search_string";
	SearchTabPagerAdapter mAdapter;
	protected String mLastMainSelectedMenu = "";

	// search typing delay timer
	private Timer timer = new Timer();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		ButterKnife.bind(this);

		setSupportActionBar(toolbar);
		ActionBar appBar = getSupportActionBar();
		if (appBar != null) {
			appBar.setTitle(getResources().getString(R.string.search_title));
			appBar.setHomeButtonEnabled(true);
			appBar.setDisplayHomeAsUpEnabled(true);
		}

		// init setting parser
		mParser = RawParser.getInstance();
		mParser.getSetting(this);

		mQueryHelper = QueryHelper.getInstance();
		if (mQueryHelper.getRealm() == null) {
			RealmConfiguration config = new RealmConfiguration.Builder(this)
					.name("phrasebook.realm")
					.schemaVersion(1)
					.build();
			mQueryHelper.setRealm(Realm.getInstance(config));
		}
		mQueryHelper.getContents("SearchXXX");

		Bundle b = getIntent().getExtras();
//		if (b != null) {
			mLastMainSelectedMenu = b.getString(MainActivity.SELECTED_KEY);
//		}

//		mFragment = new SearchActivityFragment();

//		getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, mFragment).commit();

		updateTab();

		if (savedInstanceState != null && savedInstanceState.containsKey(SEARCH_KEY)) {
//			mFragment._lastExpandedPost = savedInstanceState.getInt(LAST_POST_KEY);
			mSearch = savedInstanceState.getString(SEARCH_KEY);
		}

	}

	private void updateTab() {
		mAdapter = new SearchTabPagerAdapter(getSupportFragmentManager(), this);
		viewPager.setAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();

		tabLayout.setTabsFromPagerAdapter(mAdapter);
		tabLayout.setupWithViewPager(viewPager);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString(SEARCH_KEY, searchView.getQuery().toString());
//		outState.putInt(LAST_POST_KEY, mFragment._lastExpandedPost);
		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_search, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem item = menu.findItem(R.id.action_search);
		searchView = (SearchView) MenuItemCompat.getActionView(item);
		searchView.setIconifiedByDefault(false);
		arrangeSearchView();
		if (!mSearch.isEmpty()) {
			searchView.setQuery(mSearch, false);
			filterFragments(mSearch);
//			mFragment.filterData(mSearch);
//			mFragment.expandLastPos();
		}
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
//				mFragment.filterData(query);
				filterFragments(query);
				return true;
			}

			@Override
			public boolean onQueryTextChange(final String newText) {
//				mFragment.filterData(newText);

				if( timer != null ) {
					timer.cancel();
				}

				if (newText.length() >= 5) {

					timer = new Timer();
					timer.schedule(new TimerTask() {
						@Override
						public void run() {
							GoogleAnalytics.getInstance(SearchActivity.this).sendAction("Users", "Search", newText, 1);
						}
					}, mParser.setting.search_query_delay);
				}

				filterFragments(newText);
				return true;
			}
		});
		return super.onPrepareOptionsMenu(menu);
	}

	private void arrangeSearchView() {
		try {
			Resources resources = this.getResources();
			TextView autoComplete = (TextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
			Class<?> clazz = Class.forName("android.widget.SearchView$SearchAutoComplete");
			Method textSizeMethod = clazz.getMethod("getTextSize");
			Float rawTextSize = (Float) textSizeMethod.invoke(autoComplete);
			int textSize = (int) (rawTextSize * 1.25);

			Drawable searchIcon = resources.getDrawable(R.drawable.ic_action_search);
			searchIcon.setBounds(0, 0, textSize, textSize);

			SpannableStringBuilder stopHint = new SpannableStringBuilder("   ");
//			stopHint.append(this.getString(R.string.search_quotes));
			stopHint.append(getResources().getString(R.string.search_hint));
			stopHint.setSpan(new ImageSpan(searchIcon), 1, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

			Method setHintMethod = clazz.getMethod("setHint", CharSequence.class);
			setHintMethod.invoke(autoComplete, stopHint);

//			Remove magnifier icon
			ImageView img = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
			img.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void filterFragments(String search) {
		for (int i = 0; i < mAdapter.getCount(); i++) {
			mAdapter.getFragment(i).filterData(search);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == android.R.id.home) {
//			SoundPlayer.getInstance().stopSequence();
			onBackPressed();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ButterKnife.unbind(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		EventBus.getDefault().unregister(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		SoundPlayer.getInstance().stopSequence();
	}

	public void onEvent(FavClickEvent e) {
		Snackbar sb;
		if (e.state) {
			sb = Snackbar.make(
					activitySearch,
					String.format(getResources().getString(R.string.favorite_notif), e.english),
					Snackbar.LENGTH_SHORT
			);
		} else {
			sb = Snackbar.make(
					activitySearch,
					String.format(getResources().getString(R.string.unfavorite_notif), e.english),
					Snackbar.LENGTH_SHORT
			);
		}

		View sbView = sb.getView();
		TextView tView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
		tView.setTextColor(getResources().getColor(R.color.colorTextTabSelected));
		sb.show();
	}

	@Override
	public void onBackPressed() {
		SoundPlayer.getInstance().stopSequence();
		Timber.v("menu " + mLastMainSelectedMenu);
		mQueryHelper.getContents(mLastMainSelectedMenu);
		finish();
//		super.onBackPressed();
	}
}
