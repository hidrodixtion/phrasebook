package com.projectbox.phrasebook.customview;

import android.content.Context;
import android.support.v4.widget.SlidingPaneLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by hidrodixtion on 11/3/15.
 */
public class MarginSlidePaneLayout extends SlidingPaneLayout {
	public MarginSlidePaneLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if ( this.isOpen() || (ev.getX() < (getWidth() * 0.25)) ) {
			return super.onInterceptTouchEvent(ev);
		}
		return false;
	}
}
