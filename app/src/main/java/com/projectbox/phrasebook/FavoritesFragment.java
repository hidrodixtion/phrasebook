package com.projectbox.phrasebook;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.projectbox.phrasebook.adapter.SingleListAdapter;
import com.projectbox.phrasebook.data.QueryHelper;
import com.projectbox.phrasebook.event.FavClickEvent;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

public class FavoritesFragment extends SingleListFragment {
	@Bind(R.id.list_view)
	ExpandableListView listView;
	@Bind(R.id.txt_blank_favorites)
	TextView txtBlankFavorites;

	public FavoritesFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_favorites, container, false);
		ButterKnife.bind(this, view);

		txtBlankFavorites.setText(getText(R.string.blank_favorites));

		Bundle bundle = getArguments();
		mSearch = bundle.getString("search");
//		mTitle = bundle.getString("title");
		mTitle = "Favorites";

		Timber.v(" >> " + mTitle);

		mQueryHelper = QueryHelper.getInstance();
		mQueryHelper.getFavorites();
		mPhrases = mQueryHelper.getFavoritePhrases();

		if (mPhrases.size() == 0) {
			txtBlankFavorites.setVisibility(View.VISIBLE);
			listView.setVisibility(View.GONE);
		} else {
			txtBlankFavorites.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
		}

		adapter = new SingleListAdapter(getActivity(), mPhrases);

		init();

		return view;
	}

	public void onEvent(FavClickEvent e) {
		if (!e.state) {
			adapter.mPhrases = mQueryHelper.getFavoritePhrases();
			adapter.notifyDataSetChanged();

			if (adapter.mPhrases.size() == 0) {
				btnPlayAll.setVisibility(View.GONE);
				txtBlankFavorites.setVisibility(View.VISIBLE);
				listView.setVisibility(View.GONE);
			} else {
				btnPlayAll.setVisibility(View.VISIBLE);
				txtBlankFavorites.setVisibility(View.GONE);
				listView.setVisibility(View.VISIBLE);
			}
		}
	}
}
