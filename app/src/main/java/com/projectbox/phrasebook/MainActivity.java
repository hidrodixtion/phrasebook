package com.projectbox.phrasebook;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.projectbox.phrasebook.adapter.DrawerAdapter;
import com.projectbox.phrasebook.adapter.TabPagerAdapter;
import com.projectbox.phrasebook.customview.MarginSlidePaneLayout;
import com.projectbox.phrasebook.data.PreferenceHelper;
import com.projectbox.phrasebook.data.QueryHelper;
import com.projectbox.phrasebook.data.RawParser;
import com.projectbox.phrasebook.data.model.Content;
import com.projectbox.phrasebook.event.FavClickEvent;
import com.projectbox.phrasebook.util.IabHelper;
import com.projectbox.phrasebook.util.IabResult;
import com.projectbox.phrasebook.util.Inventory;
import com.projectbox.phrasebook.util.Purchase;
import com.projectbox.phrasebook.utility.SoundPlayer;
import com.projectbox.phrasebook.utility.TabLayoutHelper;
import com.tjeannin.apprate.AppRate;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

public class MainActivity extends BaseActivity {
	public static String PACKAGE_NAME;
	public static final int REQ_SETTING = 100;
	public static final int RES_SETTING = 101;
	public static final int REQ_SHOP = 200;
	public static final int RES_SHOP = 201;
	public static final String SELECTED_KEY = "selected_menu";

	@Bind(R.id.app_bar)
	Toolbar toolbar;
	@Bind(R.id.app_tab)
	TabLayout appTab;
	@Bind(R.id.activity_main)
	LinearLayout activityMain;
	@Bind(R.id.view_pager)
	ViewPager viewPager;
	@Bind(R.id.adView)
	AdView adView;
	@Bind(R.id.img_rid_ad)
	ImageView imgRidAd;
	@Bind(R.id.list_drawer)
	ListView listDrawer;
	@Bind(R.id.layout_pane)
	MarginSlidePaneLayout paneLayout;
	@Bind(R.id.layout_drawer_list)
	FrameLayout drawerFrameLayout;
	@Bind(R.id.layout_overlay)
	RelativeLayout overlayLayout;

	RawParser mParser;
	int _lastSelectedDrawerIndex = 0;
	String _lastSelectedMenuString;
	TabPagerAdapter _adapter;
	boolean isIabSetup = false;
	private PreferenceHelper _prefHelper;
	private String stringSearch = "";
	private IabHelper mHelper;
	private Realm mRealm;
	private QueryHelper mQHelper;
	private DrawerAdapter drawerAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		PACKAGE_NAME = getPackageName();

		Timber.plant(new Timber.DebugTree());

		setSupportActionBar(toolbar);
		ActionBar appBar = getSupportActionBar();
		appBar.setTitle(getResources().getString(R.string.my_favorites));

		if (appBar != null) {
			appBar.setHomeAsUpIndicator(R.drawable.ic_action_menu);
			appBar.setDisplayHomeAsUpEnabled(true);
		}

		Timber.v(getResources().getConfiguration().smallestScreenWidthDp + "");

		_prefHelper = PreferenceHelper.getInstance();
		_prefHelper.setContext(this);

		initRealm();

		_lastSelectedMenuString = getResources().getString(R.string.my_favorites);
		if (savedInstanceState != null && savedInstanceState.containsKey(SELECTED_KEY)) {
			_lastSelectedMenuString = savedInstanceState.getString(SELECTED_KEY);
		}

		Timber.v("saved instance: " + savedInstanceState);

		updateTabAdapter();

		mParser = RawParser.getInstance();
		mParser.getSetting(this);
		mParser.getIAB(this);

		// Initialize Facebook SDK
		if (mParser.setting.enable_fb_stats) {
			FacebookSdk.sdkInitialize(getApplicationContext());
		}

		int appOpenedCount = _prefHelper.getAppOpenCount(this);
//		if (!((PhrasebookApplication) getApplication()).isLastConfigChanged(10000)) {
//			_prefHelper.setAppOpenCount(this, appOpenedCount + 1);
//		}

		Timber.v("state: " + savedInstanceState);

		if (!_prefHelper.getUnlockedCategory(this).contains("ads")) {
			if (savedInstanceState != null) appOpenedCount -= 1;

			if (appOpenedCount % mParser.setting.ad.switch_after == 0) {
				imgRidAd.setVisibility(View.VISIBLE);
				adView.setVisibility(View.GONE);
			} else {
				AdRequest req = new AdRequest.Builder().build();
				adView.setAdListener(new AdListener() {
					@Override
					public void onAdFailedToLoad(int errorCode) {
						if (imgRidAd != null) {
							imgRidAd.setVisibility(View.VISIBLE);
							adView.setVisibility(View.GONE);
						}
					}
				});
				adView.loadAd(req);
				adView.setVisibility(View.VISIBLE);

				imgRidAd.setVisibility(View.GONE);
			}

		}

		if (savedInstanceState == null) {
			_prefHelper.setAppOpenCount(this, appOpenedCount + 1);
		}

		paneLayout.setSliderFadeColor(getResources().getColor(android.R.color.transparent));

		initDrawerList();
		SoundPlayer.getInstance().initPlayer();

		setupIAB();

		paneLayout.setPanelSlideListener(onPaneSlide);
		overlayLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				paneLayout.closePane();
			}
		});

		// Init app rater if enabled
		if(mParser.setting.enable_app_rater) {
			AlertDialog.Builder rateDialog = new AlertDialog.Builder(this)
				.setTitle(R.string.ar_rate_dialog_title)
				.setMessage(R.string.ar_rate_dialog_message)
				.setPositiveButton(R.string.ar_rate_dialog_yes_btn, null)
				.setNegativeButton(R.string.ar_rate_dialog_no_btn, null)
				.setNeutralButton(R.string.ar_rate_dialog_later_btn, null);

			AlertDialog.Builder likeDialog = new AlertDialog.Builder(this)
				.setTitle(R.string.ar_like_dialog_title)
				.setMessage(R.string.ar_like_dialog_message)
				.setPositiveButton(R.string.ar_like_dialog_yes_btn, null)
				.setNegativeButton(R.string.ar_like_dialog_no_btn, null);

			AlertDialog.Builder feedbackDialog = new AlertDialog.Builder(this)
				.setTitle(R.string.ar_feedback_dialog_title)
				.setMessage(R.string.ar_feedback_dialog_message)
				.setPositiveButton(R.string.ar_feedback_dialog_yes_btn, null)
				.setNegativeButton(R.string.ar_feedback_dialog_no_btn, null);

			new AppRate(this)
				.setMinDaysUntilPrompt(mParser.setting.ar_min_days)
				.setMinLaunchesUntilPrompt(mParser.setting.ar_min_launches)
				.setShowIfAppHasCrashed(false)
				.showDoYouLikeTheAppFlow(mParser.setting.supportEmail)
				.setCustomDialog(rateDialog)
				.setCustomDoYouLikeAppDialog(likeDialog)
				.setCustomSendFeedbackDialog(feedbackDialog)
				.init();
		}
	}

	private void setCurrentVolume() {
		AudioManager audioMan = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		float currentMusicVolume = (float) audioMan.getStreamVolume(AudioManager.STREAM_MUSIC) / audioMan.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//		Timber.v(audioMan.getStreamVolume(AudioManager.STREAM_MUSIC) + " " + audioMan.getStreamMaxVolume(AudioManager.STREAM_MUSIC) + " " + currentMusicVolume + "");
		_prefHelper.setVolume(currentMusicVolume);
	}

	protected void initRealm() {
		RealmConfiguration config = new RealmConfiguration.Builder(this)
				.name("phrasebook.realm")
				.schemaVersion(1)
				.build();
		mRealm = Realm.getInstance(config);

		if (_prefHelper.isVersionChanged(this)) {
			InputStream is = this.getResources().openRawResource(R.raw.data);

			try {
				mRealm.beginTransaction();
				mRealm.createOrUpdateAllFromJson(Content.class, is);
				mRealm.commitTransaction();
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		mQHelper = QueryHelper.getInstance();
		mQHelper.setRealm(mRealm);
		mQHelper.createPhraseContentBacklink();
	}

	protected void setupIAB() {
		mHelper = new IabHelper(this, mParser.setting.appKey);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			@Override
			public void onIabSetupFinished(IabResult result) {
				if (result.isSuccess()) {
					isIabSetup = true;
					Timber.v("> IAB Success Setup");
					mHelper.queryInventoryAsync(mGotInventoryListener);
				} else if (result.isFailure()) {
					Timber.v("> IAB Success Failure");
				}
			}
		});
	}

	IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		@Override
		public void onQueryInventoryFinished(IabResult result, Inventory inv) {
			if (result.isFailure()) {
				Timber.e("failed to query");
				return;
			}

			ArrayList<String> arrList = new ArrayList(inv.getAllOwnedSkus());
			if (arrList.size() > 0)
				Timber.v(inv.getAllOwnedSkus().get(0));
			_prefHelper.reInitUnlockedIds(MainActivity.this, arrList);

			for (String item : arrList) {
				_prefHelper.addUnlockedCategory(MainActivity.this, mParser.getUnlockedCategory(item));
			}

			if (_prefHelper.getUnlockedCategory(MainActivity.this).contains("ads")) {
				imgRidAd.setVisibility(View.GONE);
				adView.setVisibility(View.GONE);
			}

			updateDb();
		}
	};

	private void updateDb() {
		ArrayList<String> categories = _prefHelper.getUnlockedCategory(this);
		for (String category : categories) {
			Timber.v(" >> update: " + category);
			QueryHelper.getInstance().markAsPaid(category);
		}
		drawerAdapter.refreshData();
	}

	protected void initDrawerList() {
		drawerAdapter = new DrawerAdapter(this, mQHelper.getAllContents());
		listDrawer.setAdapter(drawerAdapter);
		listDrawer.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
		listDrawer.setItemChecked(_lastSelectedDrawerIndex, true);

		drawerFrameLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Timber.v("Pane Status Open? " + paneLayout.isOpen());
				if (paneLayout.isOpen())
					paneLayout.closePane();
				else
					paneLayout.openPane();
			}
		});

		listDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				SoundPlayer.getInstance().stopSequence();

				String selectedTitle = drawerAdapter.getItem(i);
				if (selectedTitle.equals(getResources().getString(R.string.more_categories))) {
					startActivity(new Intent(MainActivity.this, ShopActivity.class));
					listDrawer.setItemChecked(_lastSelectedDrawerIndex, true);
					return;
				}

				toolbar.setTitle(selectedTitle);

				if (!_lastSelectedMenuString.equals(selectedTitle)) {
					_lastSelectedMenuString = selectedTitle;

					updateTabAdapter();
				}

				_lastSelectedDrawerIndex = i;
			}
		});
	}

	private void updateTabAdapter() {
		mQHelper.getContents(_lastSelectedMenuString);

		if (mQHelper.getContents().size() == 0) {
			appTab.setVisibility(View.GONE);
		} else {
			if (mQHelper.getContents().get(0).getSubcontents().size() > 1) {
				appTab.setVisibility(View.VISIBLE);
			} else {
				appTab.setVisibility(View.GONE);
			}
		}

		_adapter = new TabPagerAdapter(MainActivity.this.getSupportFragmentManager(), stringSearch, _lastSelectedMenuString, mQHelper.getContents());
		viewPager.setAdapter(_adapter);
//		_adapter.notifyDataSetChanged();

//		appTab.setTabsFromPagerAdapter(_adapter);
//		Timber.v(">> tab count: " + appTab.getTabCount() + " " + appTab.getSelectedTabPosition());
//		appTab.setupWithViewPager(viewPager);

		// setup tablayout helper
		TabLayoutHelper tablayoutHelper = new TabLayoutHelper(appTab, viewPager);
		tablayoutHelper.setAutoAdjustTabModeEnabled(true);

		toolbar.setTitle(_lastSelectedMenuString);
	}

	SlidingPaneLayout.PanelSlideListener onPaneSlide = new SlidingPaneLayout.PanelSlideListener() {
		@Override
		public void onPanelSlide(View panel, float slideOffset) {

		}

		@Override
		public void onPanelOpened(View panel) {
			overlayLayout.setVisibility(View.VISIBLE);
		}

		@Override
		public void onPanelClosed(View panel) {
			overlayLayout.setVisibility(View.GONE);
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
			try {
				Method m = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", Boolean.TYPE);
				m.setAccessible(true);
				m.invoke(menu, true);
			} catch (NoSuchMethodException e) {
				Timber.e("onMenuOpened-NoSuchMethod", e);
			} catch (InvocationTargetException e) {
				Timber.e("onMenuOpened-InvocationTarget", e);
			} catch (IllegalAccessException e) {
				Timber.e("onMenuOpened-IllegalAccess", e);
			}
		}
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		switch (id) {
			case R.id.action_settings:
				setCurrentVolume();
				startActivityForResult(new Intent(this, SettingsActivity.class), REQ_SETTING);
				break;
			case android.R.id.home:
				if (paneLayout.isOpen())
					paneLayout.closePane();
				else
					paneLayout.openPane();
				break;
			case R.id.action_search:
				Bundle bundle = new Bundle();
				bundle.putString(SELECTED_KEY, _lastSelectedMenuString);
				Intent intent = new Intent(this, SearchActivity.class);
				intent.putExtras(bundle);
				startActivity(intent);
				break;
			case R.id.action_shop:
				startActivityForResult(new Intent(this, ShopActivity.class), REQ_SHOP);
				break;
			case R.id.action_rating:
				try {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mParser.setting.appID)));
				} catch (ActivityNotFoundException e) {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + mParser.setting.appID)));
				}
				break;
			case R.id.action_feedback:
				Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", mParser.setting.supportEmail, null));
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
				startActivity(Intent.createChooser(emailIntent, getResources().getString(R.string.feedback_title)));
				break;
			case R.id.action_sharing:
				Intent shareIntent = new Intent(Intent.ACTION_SEND);
				shareIntent.setType("text/plain");
				shareIntent.putExtra(Intent.EXTRA_TEXT, mParser.setting.appURL);
				startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.share_title)));
				break;
		}

		if (id != android.R.id.home) {
			_prefHelper.setAppOpenCount(this, _prefHelper.getAppOpenCount(this) - 1);
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
			switch (requestCode) {
				case REQ_SETTING:
					Timber.v("volume: " + _prefHelper.getVolume() + "");
					if (resultCode == RES_SETTING) {
						AudioManager audioMan = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
						float volume = audioMan.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * _prefHelper.getVolume();
						audioMan.setStreamVolume(AudioManager.STREAM_MUSIC, (int) Math.ceil(volume), 0);
					}
					break;
				case REQ_SHOP:
					if (resultCode == RES_SHOP) {
						if (_prefHelper.getUnlockedCategory(this).contains("ads")) {
							imgRidAd.setVisibility(View.GONE);
							adView.setVisibility(View.GONE);
						}

						drawerAdapter.refreshData();
					}
					break;
			}

			Timber.v("result " + requestCode + " " + resultCode);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString(SELECTED_KEY, _lastSelectedMenuString);
		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onMenuOpened(int featureId, Menu menu) {
		return super.onMenuOpened(featureId, menu);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Timber.uprootAll();
		ButterKnife.unbind(this);
		if (mHelper != null) {
			mHelper.dispose();
		}
		mHelper = null;
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		EventBus.getDefault().unregister(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mParser.setting.enable_fb_stats) {
			AppEventsLogger.activateApp(this);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		SoundPlayer.getInstance().stopSequence();
		if (mParser.setting.enable_fb_stats) {
			AppEventsLogger.deactivateApp(this);
		}
	}

	@OnClick(R.id.img_rid_ad)
	public void onRidAdsClick() {
		if (!isIabSetup) {
			mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
				@Override
				public void onIabSetupFinished(IabResult result) {
					if (result.isSuccess()) {
						purchaseRemoveAd();
					} else {

					}
				}
			});
		} else {
			purchaseRemoveAd();
		}
	}

	private void purchaseRemoveAd() {
		mHelper.launchPurchaseFlow(this, mParser.SKU_ads, 10001, new IabHelper.OnIabPurchaseFinishedListener() {
			@Override
			public void onIabPurchaseFinished(IabResult result, Purchase info) {
				if (result.isSuccess()) {
					if (info.getSku().equals(mParser.SKU_ads)) {
						_prefHelper.addUnlockedID(MainActivity.this, mParser.SKU_ads);
						_prefHelper.addUnlockedCategory(MainActivity.this, mParser.getUnlockedCategory(mParser.SKU_ads));

						adView.destroy();
						adView.setVisibility(View.GONE);
						imgRidAd.setVisibility(View.GONE);
					}
				}
			}
		}, "mypurchasetoken2");
	}

	public void onEvent(FavClickEvent e) {
		Snackbar sb;
		if (e.state) {
			sb = Snackbar.make(
					activityMain,
					String.format(getResources().getString(R.string.favorite_notif), e.english),
					Snackbar.LENGTH_SHORT
			);
		} else {
			sb = Snackbar.make(
					activityMain,
					String.format(getResources().getString(R.string.unfavorite_notif), e.english),
					Snackbar.LENGTH_SHORT
			);
		}

		View sbView = sb.getView();
		TextView tView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
		tView.setTextColor(getResources().getColor(R.color.colorTextTabSelected));
		sb.show();
	}
}
