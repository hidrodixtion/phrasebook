package com.projectbox.phrasebook;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by hidrodixtion on 7/1/15.
 */
public class SplashActivity extends BaseActivity {
	private static final int SPLASH_TIME_OUT = 2000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent i = new Intent(SplashActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);

				finish();
			}
		}, SPLASH_TIME_OUT);
	}
}
