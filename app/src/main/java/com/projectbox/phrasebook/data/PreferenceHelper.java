package com.projectbox.phrasebook.data;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.projectbox.phrasebook.data.model.ModelPhrase;
import com.projectbox.phrasebook.utility.TinyDB;

import java.util.ArrayList;



/**
 * Created by hidrodixtion on 6/28/15.
 */
public class PreferenceHelper {
	private TinyDB _tb;
	private ModelPhrase _model = new ModelPhrase();
	private ModelPhrase.PhraseTitle _title = new ModelPhrase.PhraseTitle();


	private static class Holder {
		private static final PreferenceHelper INSTANCE = new PreferenceHelper();
	}

	private PreferenceHelper() {
		_title.title = "Favorites";
		_title.phrases = new ArrayList<>();

		_model.data = new ArrayList<>();
		_model.data.add(_title);
	}

	public static PreferenceHelper getInstance() {
		return Holder.INSTANCE;
	}

	public void setContext(Context context) {
		if (_tb == null)
			_tb = new TinyDB(context);
	}

	public Boolean isVersionChanged(Context context) {
		if (_tb == null) {
			_tb = new TinyDB(context);
		}

		try {
			PackageInfo pkgInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			int vcode = pkgInfo.versionCode;

			if (_tb.getInt("app_version", 0) != vcode) {
				_tb.putInt("app_version", vcode);
				return true;
			} else {
				return false;
			}
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}

		return false;
	}

	public int getAppOpenCount(Context context) {
		if (_tb == null) {
			_tb = new TinyDB(context);
		}

		return _tb.getInt("app_open", 1);
	}

	public void setAppOpenCount(Context context, int value) {
		if (_tb == null) {
			_tb = new TinyDB(context);
		}

		if (value == Integer.MAX_VALUE - 1) value = 0;
		_tb.putInt("app_open", value);
	}

	public ArrayList<String> getUnlockedIDs(Context context) {
		if (_tb == null) {
			_tb = new TinyDB(context);
		}

		return _tb.getListString("unlock");
	}

	public void reInitUnlockedIds(Context context, ArrayList<String> values) {
		if (_tb == null) { _tb = new TinyDB(context); }

		_tb.putListString("unlock", values);
		_tb.putListString("unlock_categories", new ArrayList<String>());
	}

	public void addUnlockedID(Context context, String id) {
		if (_tb == null) {
			_tb = new TinyDB(context);
		}

		ArrayList<String> list = getUnlockedIDs(context);
		list.add(id);
		_tb.putListString("unlock", list);
	}

	public void addUnlockedCategory(Context context, ArrayList<String> category) {
		if (_tb == null) {
			_tb = new TinyDB(context);
		}

		ArrayList<String> arr = _tb.getListString("unlock_categories");
		arr.addAll(category);
		_tb.putListString("unlock_categories", arr);
	}

	public ArrayList<String> getUnlockedCategory(Context context) {
		if (_tb == null) {
			_tb = new TinyDB(context);
		}

		return _tb.getListString("unlock_categories");
	}

	public boolean isAutoPlay() {
		return _tb.getBoolean("pref_play_auto", true);
	}

	public float getVolume() { return _tb.getFloat("pref_volume", 0); }
	public void setVolume(float value) {
		_tb.putFloat("pref_volume", value);
	}
}
