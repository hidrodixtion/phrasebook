package com.projectbox.phrasebook.data;

import com.projectbox.phrasebook.data.model.Content;
import com.projectbox.phrasebook.data.model.Phrase;
import com.projectbox.phrasebook.data.model.SubContent;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import timber.log.Timber;

/**
 * Created by hidrodixtion on 7/23/15.
 */
public class QueryHelper {
	private RealmResults<Content> mContents;

	public RealmResults<Content> getContents() {
		return mContents;
	}

	private RealmResults<SubContent> mSubContents;

	public RealmResults<SubContent> getSubContents() {
		return mSubContents;
	}

	private RealmResults<Phrase> mFavoritePhrases;

	public RealmResults<Phrase> getFavoritePhrases() {
		return mFavoritePhrases;
	}

	public RealmResults<Phrase> getAllPhrases() {
		return mRealm.where(Phrase.class).findAll();
	}

	private Realm mRealm;

	public void setRealm(Realm realm) {
		this.mRealm = realm;
	}

	public Realm getRealm() {
		return mRealm;
	}

	private static class Holder {
		private static final QueryHelper INSTANCE = new QueryHelper();
	}

	private QueryHelper() {

	}

	public static QueryHelper getInstance() {
		return Holder.INSTANCE;
	}

	public RealmResults<Content> getAllContents() {
		return mRealm.where(Content.class).findAll();
	}

	public void getContents(String title) {
		mContents = mRealm.where(Content.class).equalTo("title", title).findAll();

		if (mContents.size() > 0)
			Timber.v(mContents.get(0).getTitle() + "");
	}

	public RealmResults<Phrase> getAllFreePhrases() {
		return mRealm.where(Phrase.class).equalTo("contentParent.free", true).findAll();
	}

	public void getSubContents(String title) {
		mSubContents = mRealm.where(SubContent.class).equalTo("title", title).findAll();
	}

	public void createPhraseContentBacklink() {
		mContents = getAllContents();

		mRealm.beginTransaction();
		for (int ic = 0; ic < mContents.size(); ic++) {
			RealmList<SubContent> subcontents = mContents.get(ic).getSubcontents();
			for (int is = 0; is < subcontents.size(); is++) {
				RealmList<Phrase> phrases = subcontents.get(is).getPhrases();
				for (int ip = 0; ip < phrases.size(); ip++) {
					phrases.get(ip).setContentParent(mContents.get(ic));
				}
			}
		}
		mRealm.commitTransaction();
	}
	
	public void getFavorites() {
		mFavoritePhrases = mRealm.where(Phrase.class).equalTo("favorite", true).findAll();
		mFavoritePhrases.sort("favoriteDate", RealmResults.SORT_ORDER_ASCENDING);
	}

	public void markAsFavorite(Phrase phrase, boolean favState) {
		if (phrase != null) {
			Calendar c = Calendar.getInstance(Locale.getDefault());
			Date now = c.getTime();

			mRealm.beginTransaction();
			phrase.setFavorite(favState);
			phrase.setFavoriteDate(now);
			mRealm.commitTransaction();
		}
	}

	public void markAsPaid(String title) {
		Content content = mRealm.where(Content.class).equalTo("title", title).findFirst();

		if (content == null) {
			return;
		}

		Timber.v(content.getTitle());
		mRealm.beginTransaction();
		content.setFree(true);
		mRealm.commitTransaction();
	}

	public RealmList<Phrase> getFavoritePhrases(int position) {
		return mSubContents.get(position).getPhrases();
	}
}
