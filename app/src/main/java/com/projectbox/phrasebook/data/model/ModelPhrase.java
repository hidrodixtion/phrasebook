package com.projectbox.phrasebook.data.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.ArrayList;

/**
 * Created by hidrodixtion on 6/15/15.
 */
@JsonObject
public class ModelPhrase {
	@JsonField
	public ArrayList<PhraseTitle> data;

	@JsonObject
	public static final class PhraseTitle {
		@JsonField
		public String title;

		@JsonField
		public ArrayList<Phrase> phrases;

		@JsonObject
		public static final class Phrase {
			@JsonField
			public String english;

			@JsonField
			public String translate;

			@JsonField
			public String romaji;

			@JsonField
			public String sound;
		}
	}
}
