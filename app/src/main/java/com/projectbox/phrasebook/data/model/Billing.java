package com.projectbox.phrasebook.data.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.ArrayList;

/**
 * Created by hidrodixtion on 8/21/15.
 */
@JsonObject
public class Billing {
	@JsonField
	public String title;

	@JsonField
	public String description;

	@JsonField
	public String id;

	@JsonField
	public String icon;

	@JsonField
	public ArrayList<String> unlock;

	@JsonField
	public String price;
}
