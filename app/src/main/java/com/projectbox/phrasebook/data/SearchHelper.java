package com.projectbox.phrasebook.data;

/**
 * Created by hidrodixtion on 10/7/15.
 */
public class SearchHelper {
	private String mSearch = "";

	public String getSearch() {
		return mSearch;
	}

	public void setSearch(String mSearch) {
		this.mSearch = mSearch;
	}

	private static class Holder {
		private static final SearchHelper INSTANCE = new SearchHelper();
	}

	public static SearchHelper getInstance() {return Holder.INSTANCE;}
}
