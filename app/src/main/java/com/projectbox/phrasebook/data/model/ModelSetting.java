package com.projectbox.phrasebook.data.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by hidrodixtion on 8/13/15.
 */
@JsonObject
public class ModelSetting {
	@JsonField(name = "app_key")
	public String appKey;

	@JsonField(name = "app_id")
	public String appID;

	@JsonField(name = "app_url")
	public String appURL;

	@JsonField(name = "support_email")
	public String supportEmail;

	@JsonField(name = "enable_ga_stats")
	public boolean enable_ga_stats;

	@JsonField(name = "enable_fb_stats")
	public boolean enable_fb_stats;

	@JsonField(name = "search_query_delay")
	public int search_query_delay;

	@JsonField(name = "enable_app_rater")
	public boolean enable_app_rater;

	@JsonField(name = "ar_min_days")
	public int ar_min_days;

	@JsonField(name = "ar_min_launches")
	public int ar_min_launches;

	@JsonField
	public Ad ad;

	@JsonObject
	public static final class Ad {
		@JsonField(name = "switch")
		public int switch_after;
	}
}
