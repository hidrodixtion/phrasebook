package com.projectbox.phrasebook.data.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hidrodixtion on 7/23/15.
 */
public class SubContent extends RealmObject {
	@PrimaryKey
	private String id;

	private String title;
	private RealmList<Phrase> phrases;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public RealmList<Phrase> getPhrases() {
		return phrases;
	}

	public void setPhrases(RealmList<Phrase> phrases) {
		this.phrases = phrases;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
