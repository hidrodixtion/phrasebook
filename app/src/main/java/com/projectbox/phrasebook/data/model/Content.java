package com.projectbox.phrasebook.data.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hidrodixtion on 7/23/15.
 */
public class Content extends RealmObject {

	@PrimaryKey
	private int id;

	private String title;
	private RealmList<SubContent> subcontents;
	private boolean free;
	private String icon;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public RealmList<SubContent> getSubcontents() {
		return subcontents;
	}

	public void setSubcontents(RealmList<SubContent> subcontents) {
		this.subcontents = subcontents;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isFree() { return free; }
	public void setFree(boolean value) { this.free = value; }

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
}
