package com.projectbox.phrasebook.data;

import android.content.Context;

import com.bluelinelabs.logansquare.LoganSquare;
import com.projectbox.phrasebook.R;
import com.projectbox.phrasebook.data.model.Billing;
import com.projectbox.phrasebook.data.model.ModelSetting;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by hidrodixtion on 6/15/15.
 */
public class RawParser {
	private static class Holder {
		private static final RawParser INSTANCE = new RawParser();
	}

	public static RawParser getInstance() {
		return Holder.INSTANCE;
	}

	public ModelSetting setting;
	public List<Billing> billings;
	public String SKU_ads;

	public void getSetting(Context context) {
		try {
			InputStream is = context.getResources().openRawResource(R.raw.setting);
			setting = LoganSquare.parse(is, ModelSetting.class);
		} catch (IOException e) {
			Timber.e("Can't read setting");
		}
	}

	public void getIAB(Context context) {
		try {
			InputStream is = context.getResources().openRawResource(R.raw.iab);
			billings = LoganSquare.parseList(is, Billing.class);
			for (Billing billing : billings) {
				if (billing.unlock.size() == 0) continue;

				if (billing.unlock.get(0).toLowerCase().equals("ads")) {
					SKU_ads = billing.id;
					return;
				}
			}
		} catch (IOException e) {
			Timber.e("Can't read iab");
		}
	}

	public ArrayList<String> getUnlockedCategory(String id) {
		Timber.v("billings size : " + billings.size());
		for (int i = 0; i < billings.size(); i++) {
			if (billings.get(i).id.equals(id)) {
				Timber.v("recognized id: " + billings.get(i).id);
				return billings.get(i).unlock;
			} else {
				Timber.e("This id is not recognized : " + id);
			}
		}
//		for (Billing billing : billings) {
//
//			if (billing.id.equals(id)) {
//
//			}
//		}
		return null;
	}

	public Billing getBillingByID(String id) {
		Timber.v("billings size : " + billings.size());
		for (int i = 0; i < billings.size(); i++) {
			if (billings.get(i).id.equals(id)) {
				Timber.v("recognized id: " + billings.get(i).id);
				return billings.get(i);
			} else {
				Timber.e("This id is not recognized : " + id);
			}
		}
		return null;
	}
}
