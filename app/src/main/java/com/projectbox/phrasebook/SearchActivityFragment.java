package com.projectbox.phrasebook;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.projectbox.phrasebook.adapter.SearchEnglishAdapter;
import com.projectbox.phrasebook.adapter.SearchTranslationAdapter;
import com.projectbox.phrasebook.data.QueryHelper;
import com.projectbox.phrasebook.data.SearchHelper;
import com.projectbox.phrasebook.utility.SoundPlayer;
import com.software.shell.fab.ActionButton;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class SearchActivityFragment extends SingleListFragment {

	public static String TRANSLATION_KEY = "use_translation";

	@Bind(R.id.list_view)
	ExpandableListView listView;
	@Bind(R.id.txt_blank_favorites)
	TextView txtBlankFavorites;
	@Bind(R.id.btn_play_all)
	ActionButton btnPlayAll;

	boolean mIsTranslation = false;

	public SearchActivityFragment() {
		super();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_favorites, container, false);
		ButterKnife.bind(this, view);

		txtBlankFavorites.setText("");
		txtBlankFavorites.setGravity(Gravity.CENTER);
		mSearch = "";

		Bundle bundle = getArguments();
		if (bundle != null) {
			mIsTranslation = bundle.getBoolean(TRANSLATION_KEY, false);
			mTitle = bundle.getString("title");
		}

		setInitialVisibility(false);

		mQueryHelper = QueryHelper.getInstance();
		mPhrases = mQueryHelper.getAllFreePhrases();

		if (mIsTranslation)
			adapter = new SearchTranslationAdapter(getActivity(), mPhrases);
		else
			adapter = new SearchEnglishAdapter(getActivity(), mPhrases);

		init();

		return view;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		SearchHelper.getInstance().setSearch("");
		ButterKnife.unbind(this);
	}

	public void filterData(String search) {
		SoundPlayer.getInstance().stopSequence();
		btnPlayAll.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_play_arrow));
		btnPlayAll.setTag("paused");

		listView.collapseGroup(_lastExpandedPost);
		_lastExpandedPost = -1;

		SearchHelper.getInstance().setSearch(search);

		if (search.isEmpty()) {
			setInitialVisibility(false);
		} else {
			setInitialVisibility(true);
		}
		adapter.filter(search);

		mPhrases = adapter.mPhrases;

		if (adapter.getGroupCount() > 0)
			btnPlayAll.setVisibility(View.VISIBLE);
		else
			btnPlayAll.setVisibility(View.GONE);

		if (mIsTranslation) {
			for (int i = 0; i < adapter.getGroupCount(); i++) {
				listView.expandGroup(i);
			}
		}
	}

	protected void setInitialVisibility(boolean isVisibile) {
		if (isVisibile) {
			txtBlankFavorites.setVisibility(View.GONE);
			btnPlayAll.setVisibility(View.VISIBLE);
			listView.setVisibility(View.VISIBLE);
		} else {
			txtBlankFavorites.setVisibility(View.VISIBLE);
			btnPlayAll.setVisibility(View.GONE);
			listView.setVisibility(View.GONE);
		}
	}
}
