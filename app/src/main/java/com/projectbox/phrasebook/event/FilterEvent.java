package com.projectbox.phrasebook.event;

/**
 * Created by hidrodixtion on 6/30/15.
 */
public class FilterEvent {
	public final String filterText;

	public FilterEvent(String filterText) {
		this.filterText = filterText;
	}
}
