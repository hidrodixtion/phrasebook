package com.projectbox.phrasebook.event;

/**
 * Created by hidrodixtion on 8/19/15.
 */
public class PlayEvent {
	public final String title;
	public final int index;

	public PlayEvent(String title, int index) {
		this.title = title;
		this.index = index;
	}
}
