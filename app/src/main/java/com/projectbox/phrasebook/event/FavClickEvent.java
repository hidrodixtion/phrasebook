package com.projectbox.phrasebook.event;

/**
 * Created by hidrodixtion on 6/30/15.
 */
public class FavClickEvent {
	public final String english;
	public final boolean state;

	public FavClickEvent(String english, boolean state) {
		this.english = english;
		this.state = state;
	}
}
