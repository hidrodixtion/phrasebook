package com.projectbox.phrasebook.event;

/**
 * Created by hidrodixtion on 8/23/15.
 */
public class ShopEvent {
	public final String SKU;

	public ShopEvent(String SKU) {
		this.SKU = SKU;
	}
}
