package com.projectbox.phrasebook;

import android.app.Application;

import com.projectbox.phrasebook.data.RawParser;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

/**
 * This is a subclass of {@link Application} used to provide shared objects for this app, such as
 * the {@link Tracker}.
 */
public class AnalyticsApplication extends Application {
    private Tracker mTracker;

    protected RawParser mParser;

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {

        mParser = RawParser.getInstance();
        mParser.getSetting(this);

        if (mParser.setting.enable_ga_stats == false) {
            return null;
        }

        if (this.getResources().getIdentifier("global_tracker", "xml", this.getPackageName()) == 0) {
            return null;
        }

        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            // mTracker = analytics.newTracker(R.xml.global_tracker);
            mTracker = analytics.newTracker(this.getResources().getIdentifier("global_tracker", "xml", this.getPackageName()));
        }
        return mTracker;
    }
}