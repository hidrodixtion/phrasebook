package com.projectbox.phrasebook;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.projectbox.phrasebook.stats.GoogleAnalytics;

/**
 * Created by ayublin on 11/2/15.
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GoogleAnalytics.getInstance(this).sendScreenName(this.getClass().getSimpleName());
    }

}