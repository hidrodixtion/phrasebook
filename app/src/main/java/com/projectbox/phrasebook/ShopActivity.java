package com.projectbox.phrasebook;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.appevents.AppEventsLogger;
import com.projectbox.phrasebook.adapter.ShopAdapter;
import com.projectbox.phrasebook.data.PreferenceHelper;
import com.projectbox.phrasebook.data.QueryHelper;
import com.projectbox.phrasebook.data.RawParser;
import com.projectbox.phrasebook.data.model.Billing;
import com.projectbox.phrasebook.event.ShopEvent;
import com.projectbox.phrasebook.stats.GoogleAnalytics;
import com.projectbox.phrasebook.util.IabHelper;
import com.projectbox.phrasebook.util.IabResult;
import com.projectbox.phrasebook.util.Inventory;
import com.projectbox.phrasebook.util.Purchase;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import timber.log.Timber;

public class ShopActivity extends BaseActivity {

	@Bind(R.id.list_shop)
	RecyclerView listShop;
	@Bind(R.id.app_bar)
	Toolbar appBar;

	protected IabHelper mHelper;
	protected RawParser mParser;
	protected PreferenceHelper mPrefHelper;
	protected String SKUtobuy;
	protected int resultCode = 10;
	ShopAdapter adapter;
	boolean isItemPurchased = false;
	boolean isIABSetup = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop);
		ButterKnife.bind(this);

		setupIAB();
		initToolbar();
		initList();
	}

	private void initToolbar() {
		setSupportActionBar(appBar);
		ActionBar bar = getSupportActionBar();
		if (bar != null) {
			bar.setHomeButtonEnabled(true);
			bar.setDisplayHomeAsUpEnabled(true);
			bar.setTitle(getResources().getString(R.string.shop_title));
		}
	}

	private void initList() {
		adapter = new ShopAdapter(this);

		LinearLayoutManager lm = new LinearLayoutManager(this);
		lm.setOrientation(LinearLayoutManager.VERTICAL);
		listShop.setLayoutManager(lm);
		listShop.setHasFixedSize(true);

		listShop.setAdapter(adapter);
	}

	protected void setupIAB() {
		mParser = RawParser.getInstance();
		mPrefHelper = PreferenceHelper.getInstance();
		mHelper = new IabHelper(this, mParser.setting.appKey);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			@Override
			public void onIabSetupFinished(IabResult result) {
				if (result.isSuccess()) {
					isIABSetup = true;
					Timber.v("> IAB Success Setup");
				} else if (result.isFailure()) {
					Timber.v("> IAB Success Failure");
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_shop, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == android.R.id.home) {
//			setResult(resultCode);
			Timber.v(isItemPurchased + "");
			if (isItemPurchased)
				backAndRefresh();
			else
				NavUtils.navigateUpFromSameTask(this);
			return true;
		} else if (id == R.id.action_restore) {
			if (isIABSetup)
				mHelper.queryInventoryAsync(mQueryFinished);
			else {
				mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
					@Override
					public void onIabSetupFinished(IabResult result) {
						if (result.isSuccess()) {
							isIABSetup = true;
							Timber.v("> IAB Success Setup");
							mHelper.queryInventoryAsync(mQueryFinished);
						} else if (result.isFailure()) {
							Timber.v("> IAB Success Failure");
						}
					}
				});
			}
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	IabHelper.QueryInventoryFinishedListener mQueryFinished = new IabHelper.QueryInventoryFinishedListener() {
		@Override
		public void onQueryInventoryFinished(IabResult result, Inventory inv) {
			if (result.isFailure()) {
				Timber.e("Can't query inventory");
				return;
			}
		}
	};

	@Override
	public void onBackPressed() {
//		setResult(resultCode);
		if (isItemPurchased)
			backAndRefresh();
		else
			super.onBackPressed();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (!mHelper.handleActivityResult(requestCode, resultCode, data))
			super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ButterKnife.unbind(this);
		if (mHelper != null) {
			mHelper.dispose();
		}
		mHelper = null;
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		EventBus.getDefault().unregister(this);
	}

	IabHelper.OnIabPurchaseFinishedListener onIabFinish = new IabHelper.OnIabPurchaseFinishedListener() {
		@Override
		public void onIabPurchaseFinished(IabResult result, Purchase info) {
			Timber.v(result.isFailure() + "");
			if (result.isFailure()) {
				Timber.e("failed to buy " + SKUtobuy);
				return;
			}

			// Get product data
			Billing bought = mParser.getBillingByID(info.getSku());

			// FB log purchase
			if (mParser.setting.enable_fb_stats) {
				AppEventsLogger logger = AppEventsLogger.newLogger(ShopActivity.this);
				logger.logPurchase(new BigDecimal(bought.price.replace("$", "")), Currency.getInstance("USD"));
			}

			// GA log purchase
			GoogleAnalytics.getInstance(ShopActivity.this).trackPurchase(info.getSku(), info.getOrderId(), bought.title, Double.parseDouble(bought.price.replace("$", "")), getResources().getString(R.string.app_name));


			Timber.v(info.getSku());
			if (info.getSku().equals(SKUtobuy)) {
				resultCode = MainActivity.RES_SHOP;
				mPrefHelper.addUnlockedID(ShopActivity.this, SKUtobuy);
				mPrefHelper.addUnlockedCategory(ShopActivity.this, mParser.getUnlockedCategory(SKUtobuy));

				updateDb();
			}
		}
	};

	public void onEvent(ShopEvent e) {
		SKUtobuy = e.SKU;
		mHelper.launchPurchaseFlow(this, SKUtobuy, 1001, onIabFinish, "mypurchasetoken2");
	}

	private void updateDb() {
		ArrayList<String> categories = mParser.getUnlockedCategory(SKUtobuy);
		for (int i = 0; i < categories.size(); i++) {
			QueryHelper.getInstance().markAsPaid(categories.get(i));
		}
		adapter.refresh();

		isItemPurchased = true;
	}

	private void backAndRefresh() {
		Intent intent = new Intent(this, MainActivity.class);
		intent.putExtra("shop", true);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		setResult(MainActivity.RES_SHOP, intent);
		startActivity(intent);
//		finish();
	}
}
