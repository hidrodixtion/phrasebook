package com.projectbox.phrasebook.utility;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

import com.projectbox.phrasebook.data.model.Phrase;
import com.projectbox.phrasebook.event.PlayEvent;

import java.io.IOException;

import de.greenrobot.event.EventBus;
import io.realm.RealmResults;
import timber.log.Timber;

/**
 * Created by hidrodixtion on 8/19/15.
 */
public class SoundPlayer {
	private static class Holder {
		private static final SoundPlayer INSTANCE = new SoundPlayer();
	}

	public static String SINGLE_PLAYING = "single_play";

	protected int playIndex;
	protected MediaPlayer mPlayer;
	protected boolean isSequence = false;
	protected RealmResults<Phrase> mPhrases;
	protected String mTitle;
	protected Context mContext;
	public boolean isMediaPlaying = false;

	private SoundPlayer() {	}

	public static SoundPlayer getInstance() {
		return Holder.INSTANCE;
	}

	public void initPlayer() {
		mPlayer = new MediaPlayer();
		mPlayer.setOnCompletionListener(onMediaPlayerComplete);
	}

	MediaPlayer.OnCompletionListener onMediaPlayerComplete = new MediaPlayer.OnCompletionListener() {
		@Override
		public void onCompletion(MediaPlayer mediaPlayer) {
			if (!isSequence) {
				isMediaPlaying = false;
				return;
			}

			playIndex++;
			if (playIndex < mPhrases.size())
				playAudio(mPhrases.get(playIndex).getSound(), mTitle);
			else {
				isMediaPlaying = false;
				EventBus.getDefault().post(new PlayEvent(SINGLE_PLAYING, -1));
			}
		}
	};

	public void playSingle(Context context, String audioURL) {
		mContext = context;
		isSequence = false;

		EventBus.getDefault().post(new PlayEvent(SINGLE_PLAYING, -1));

		playAudio(audioURL, "");
	}

	public void playSequence(Context context, RealmResults<Phrase> phrases, String callerTitle) {
		playIndex = 0;
		mContext = context;
		mPhrases = phrases;
		mTitle = callerTitle;
		isSequence = true;

		playAudio(mPhrases.get(playIndex).getSound(), mTitle);
	}

	protected void playAudio(String audioURL, String title) {
		try {
			if (isSequence)
				EventBus.getDefault().post(new PlayEvent(title, playIndex));

			audioURL = "audio/" + audioURL;
//			Timber.v(audioURL);
			AssetFileDescriptor afd = mContext.getAssets().openFd(audioURL);
			isMediaPlaying = true;

			if (mPlayer.isPlaying()) mPlayer.stop();
			mPlayer.reset();
			mPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
			mPlayer.prepare();
			mPlayer.start();
		} catch (IOException err) {
			Timber.e("File '" + audioURL + "' not found!");
		}
	}

	public void stopSequence() {
		if (mPlayer != null) {
			if (mPlayer.isPlaying() || mPlayer.isLooping())
				mPlayer.stop();
			isSequence = false;
			isMediaPlaying = false;
//			mPlayer.release();
//			mPlayer = null;
		}
	}
}
