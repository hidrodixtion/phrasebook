package com.projectbox.phrasebook.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.projectbox.phrasebook.R;
import com.projectbox.phrasebook.data.PreferenceHelper;
import com.projectbox.phrasebook.data.RawParser;
import com.projectbox.phrasebook.data.model.Billing;
import com.projectbox.phrasebook.event.ShopEvent;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import timber.log.Timber;

/**
 * Created by hidrodixtion on 8/20/15.
 */
public class ShopAdapter extends RecyclerView.Adapter<VHShopItem> {
	protected ArrayList<Billing> mFileteredBillings;
	protected ArrayList<String> mUnlockedIDs;
	protected RawParser parser;
	protected Context mContext;

	public ShopAdapter(Context context) {
		mContext = context;
		mUnlockedIDs = PreferenceHelper.getInstance().getUnlockedIDs(context);

		parser = RawParser.getInstance();
		mFileteredBillings = new ArrayList<>();
		// add only items that's not bought
		for (int i=0; i < parser.billings.size(); i++) {
			if (!mUnlockedIDs.contains(parser.billings.get(i).id)) {
				mFileteredBillings.add(parser.billings.get(i));
			}
		}
	}

	public void refresh() {
		mUnlockedIDs = PreferenceHelper.getInstance().getUnlockedIDs(mContext);
		notifyDataSetChanged();
	}

	@Override
	public int getItemCount() {
		return parser.billings.size();
	}

	@Override
	public VHShopItem onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shop, parent, false);
		return new VHShopItem(view);
	}

	@Override
	public void onBindViewHolder(final VHShopItem holder, final int position) {
		holder.txtTitle.setText(parser.billings.get(position).title);
		holder.txtDescription.setText(parser.billings.get(position).description);
		try {
			holder.imgIcon.setImageDrawable(mContext.getResources().getDrawable(
					mContext.getResources().getIdentifier(
							parser.billings.get(position).icon,
							"drawable",
							mContext.getPackageName()
					)
			));
		} catch (Resources.NotFoundException e) {
			Timber.e("Icon not found");
		}
		holder.txtPrice.setText(parser.billings.get(position).price);

		if (mUnlockedIDs.contains(parser.billings.get(position).id)) {
			holder.txtBuy.setVisibility(View.GONE);
			holder.txtPrice.setVisibility(View.GONE);
			holder.imgBought.setVisibility(View.VISIBLE);
			holder.imgBought.setColorFilter(mContext.getResources().getColor(R.color.colorAccent));
		}

		holder.parentLayoutBuy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (holder.txtBuy.getVisibility() == View.VISIBLE) {
					EventBus.getDefault().post(
							new ShopEvent(parser.billings.get(position).id)
					);
				}
			}
		});
	}
}
