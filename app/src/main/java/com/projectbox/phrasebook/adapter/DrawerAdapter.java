package com.projectbox.phrasebook.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.projectbox.phrasebook.R;
import com.projectbox.phrasebook.data.model.Content;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import timber.log.Timber;

/**
 * Created by hidrodixtion on 8/25/15.
 */
public class DrawerAdapter extends BaseAdapter {
	protected Context mContext;
	protected RealmResults<Content> mContents;
	protected RealmResults<Content> mFreeContents;
	protected boolean isMoreCategory = false;

	public DrawerAdapter(Context context, RealmResults<Content> contents) {
		mContext = context;
		mContents = contents;

		refreshData();
	}

	public void refreshData() {
		mFreeContents = mContents.where().equalTo("free", true).findAll();

		Timber.v(mFreeContents.size() + " " + mContents.size());
		if (mFreeContents.size() < mContents.size()) {
			isMoreCategory = true;
		} else {
			isMoreCategory = false;
		}

		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		if (isMoreCategory)
			return mFreeContents.size() + 2;

//		added my favorites
		return mFreeContents.size() + 1;
	}

	@Override
	public String getItem(int i) {
		if (i == 0) {
			return mContext.getResources().getString(R.string.my_favorites);
		}

//		minus my favorites + more category
		if (isMoreCategory && i == getCount() - 1) {
			return mContext.getResources().getString(R.string.more_categories);
		}

//		minus my favorites
		return mFreeContents.get(i - 1).getTitle();
	}

	@Override
	public long getItemId(int i) {
		return i;
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		ViewHolderDrawer vh;
		if (view == null) {
			view = LayoutInflater.from(mContext).inflate(R.layout.item_drawer, viewGroup, false);

			vh = new ViewHolderDrawer(view);
			view.setTag(vh);
		}

		vh = (ViewHolderDrawer) view.getTag();

		if (getItem(i).equals(mContext.getResources().getString(R.string.more_categories))) {
			vh.iconDrawer.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_more));
			vh.txtMenu.setText(mContext.getResources().getString(R.string.more_categories));
		} else if (getItem(i).equals(mContext.getResources().getString(R.string.my_favorites))) {
			vh.iconDrawer.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_star));
			vh.txtMenu.setText(mContext.getResources().getString(R.string.my_favorites));
		} else {
			vh.iconDrawer.setImageDrawable(mContext.getResources().getDrawable(
					mContext.getResources().getIdentifier(
							mFreeContents.get(i-1).getIcon(),
							"drawable",
							mContext.getPackageName()
					)
			));
			vh.txtMenu.setText(mFreeContents.get(i-1).getTitle());
		}

		return view;
	}

	static class ViewHolderDrawer {
		@Bind(R.id.icon_drawer)
		ImageView iconDrawer;
		@Bind(R.id.txt_menu)
		TextView txtMenu;

		public ViewHolderDrawer(View view) {
			ButterKnife.bind(this, view);
		}
	}
}
