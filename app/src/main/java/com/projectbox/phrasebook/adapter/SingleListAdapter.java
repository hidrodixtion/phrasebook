package com.projectbox.phrasebook.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.projectbox.phrasebook.R;
import com.projectbox.phrasebook.data.QueryHelper;
import com.projectbox.phrasebook.data.model.Phrase;
import com.projectbox.phrasebook.event.FavClickEvent;
import com.projectbox.phrasebook.utility.SoundPlayer;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import io.realm.RealmResults;

/**
 * Created by hidrodixtion on 6/16/15.
 */
public class SingleListAdapter extends BaseExpandableListAdapter {
	public RealmResults<Phrase> mPhrases;
	protected RealmResults<Phrase> mOriginalPhrases;
	protected Context mContext;
	protected QueryHelper mQHelper;

	public SingleListAdapter(Context context, RealmResults<Phrase> data) {
		mContext = context;
		mPhrases = data;
		mOriginalPhrases = data;
		mQHelper = QueryHelper.getInstance();
	}

	@Override
	public Phrase getChild(int i, int i1) {
		return mPhrases.get(i);
	}

	@Override
	public int getGroupCount() {
		return mPhrases.size();
	}

	@Override
	public int getChildrenCount(int i) {
		return 1;
	}

	@Override
	public Phrase getGroup(int i) {
		return mPhrases.get(i);
	}

	@Override
	public long getGroupId(int i) {
		return i;
	}

	@Override
	public long getChildId(int i, int i1) {
		return i1;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
		ViewHolderGroup vh;

		if (view == null) {
			view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_single_header, viewGroup, false);

			vh = new ViewHolderGroup(view);
			view.setTag(vh);
		}

		vh = (ViewHolderGroup) view.getTag();
		vh.txtItemHeader.setText(getGroup(i).getEnglish(), TextView.BufferType.SPANNABLE);

		if (b) {
			view.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
		} else {
			view.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
		}

		return view;
	}

	@Override
	public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
		final ViewHolderContent vh;
		final Phrase phrase = getGroup(i);

		if (view == null) {
			view = LayoutInflater.from(mContext).inflate(R.layout.item_single_content, viewGroup, false);

			vh = new ViewHolderContent(view);
			view.setTag(vh);
		} else {
			vh = (ViewHolderContent) view.getTag();
		}

		vh.txtItemContent.setText(phrase.getTranslate());

		if (!phrase.getRomaji().isEmpty()) {
			vh.txtRomaji.setText(phrase.getRomaji());
			vh.txtRomaji.setVisibility(View.VISIBLE);
		} else {
			vh.txtRomaji.setVisibility(View.GONE);
		}

		vh.btnPlay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				SoundPlayer.getInstance().playSingle(mContext, phrase.getSound());
			}
		});

		final boolean isFavorite = phrase.isFavorite();

		if (isFavorite) {
			vh.btnFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_star));
		} else {
			vh.btnFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_star_border));
		}

		vh.btnFav.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!isFavorite) {
					mQHelper.markAsFavorite(phrase, true);
					vh.btnFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_star));
					EventBus.getDefault().post(new FavClickEvent(phrase.getEnglish(), true));
				} else {
					mQHelper.markAsFavorite(phrase, false);
					vh.btnFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_star_border));
					EventBus.getDefault().post(new FavClickEvent(phrase.getEnglish(), false));
				}
			}
		});
		return view;
	}

	@Override
	public boolean isChildSelectable(int i, int i1) {
		return true;
	}

	public void filter(String filterText) {
		filterText = filterText.toLowerCase();

		if (filterText.isEmpty())
			mPhrases = mOriginalPhrases;
		else
			mPhrases = mOriginalPhrases.where().beginGroup()
					.contains("english", filterText, false)
					.endGroup()
					.findAll();

		notifyDataSetChanged();
	}

	static class ViewHolderGroup {
		@Bind(R.id.txt_item_header)
		TextView txtItemHeader;

		ViewHolderGroup(View view) {
			ButterKnife.bind(this, view);
		}
	}

	static class ViewHolderContent {
		@Bind(R.id.txt_item_content) TextView txtItemContent;
		@Bind(R.id.txt_romaji) TextView txtRomaji;
		@Bind(R.id.btn_play)
		ImageButton btnPlay;
		@Bind(R.id.btn_fav) ImageButton btnFav;

		ViewHolderContent(View view) {
			ButterKnife.bind(this, view);
		}
	}
}
