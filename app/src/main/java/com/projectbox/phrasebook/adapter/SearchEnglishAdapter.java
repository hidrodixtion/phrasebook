package com.projectbox.phrasebook.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.projectbox.phrasebook.R;
import com.projectbox.phrasebook.data.SearchHelper;
import com.projectbox.phrasebook.data.model.Phrase;

import io.realm.RealmResults;

/**
 * Created by hidrodixtion on 10/7/15.
 */
public class SearchEnglishAdapter extends SingleListAdapter {
	public SearchEnglishAdapter(Context context, RealmResults<Phrase> data) {
		super(context, data);
	}

	@Override
	public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
		ViewHolderGroup vh;

		if (view == null) {
			view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_single_header, viewGroup, false);

			vh = new ViewHolderGroup(view);
			view.setTag(vh);
		}

		String searchString = SearchHelper.getInstance().getSearch();
		String titleString = getGroup(i).getEnglish();

		SpannableString text = new SpannableString(titleString);
		if (!searchString.isEmpty()) {
			int startIndex = titleString.toLowerCase().indexOf(searchString);
//			Timber.v(" >> Original : " + titleString + " , Search : " + searchString + " , index : " + startIndex);
			if (startIndex >= 0 && startIndex < titleString.length() - 1)
				text.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), startIndex, startIndex + searchString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//				text.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.colorPrimaryDark)), startIndex, startIndex + searchString.length(), 0);
		}

		vh = (ViewHolderGroup) view.getTag();
		vh.txtItemHeader.setText(text, TextView.BufferType.SPANNABLE);

		if (b) {
			view.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
		} else {
			view.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
		}

		return view;
	}
}
