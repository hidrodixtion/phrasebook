package com.projectbox.phrasebook.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.projectbox.phrasebook.FavoritesFragment;
import com.projectbox.phrasebook.SingleListFragment;
import com.projectbox.phrasebook.data.model.Content;

import io.realm.RealmResults;


/**
 * Created by hidrodixtion on 6/29/15.
 */
public class TabPagerAdapter extends FragmentStatePagerAdapter {
	private String mSearch = "";
	private String mTitle = "";
	private RealmResults<Content> mContents;

	public TabPagerAdapter(FragmentManager fm, String search, String selectedTitle, RealmResults<Content> contents) {
		super(fm);
		mSearch = search;
		mTitle = selectedTitle;
		//TODO: Remove duplicate call to getContents title
		mContents = contents;
	}

	@Override
	public Fragment getItem(int position) {
		Bundle bundle = new Bundle();
		bundle.putInt("position", position);
		bundle.putString("search", mSearch);
		bundle.putString("title", getPageTitle(position).toString());

		SingleListFragment fragment;

		if (mContents.size() > 0) {
			fragment = new SingleListFragment();
		} else {
			fragment = new FavoritesFragment();
		}

		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public int getCount() {
		if (mContents.size() > 0)
			return mContents.get(0).getSubcontents().size();
		else
			return 1;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		if (mContents.size() == 0) {
			return "";
		} else {
			return mContents.get(0).getSubcontents().get(position).getTitle();
		}
	}
}
