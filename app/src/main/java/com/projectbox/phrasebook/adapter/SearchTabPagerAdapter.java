package com.projectbox.phrasebook.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.projectbox.phrasebook.R;
import com.projectbox.phrasebook.SearchActivityFragment;


/**
 * Created by hidrodixtion on 10/7/15.
 */
public class SearchTabPagerAdapter extends FragmentStatePagerAdapter {
	protected Context mContext;
	SparseArray<SearchActivityFragment> mFragments;

	public SearchTabPagerAdapter(FragmentManager fm, Context context) {
		super(fm);
		mContext = context;
		mFragments = new SparseArray<>();
	}

	@Override
	public Fragment getItem(int position) {
		SearchActivityFragment fragment = new SearchActivityFragment();
		Bundle b = new Bundle();
		b.putString("title", getPageTitle(position).toString() + "_search");
		if (position == 1) {
			b.putBoolean(SearchActivityFragment.TRANSLATION_KEY, true);
		}
		fragment.setArguments(b);
		return fragment;
	}

	@Override
	public int getCount() {
		return 2;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		if (position == 0)
			return mContext.getResources().getString(R.string.tab_english);
		else
			return mContext.getResources().getString(R.string.tab_translation);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		SearchActivityFragment fragment = (SearchActivityFragment) super.instantiateItem(container, position);
		mFragments.put(position, fragment);
		return fragment;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		mFragments.remove(position);
		super.destroyItem(container, position, object);
	}

	public SearchActivityFragment getFragment(int position) {
		return mFragments.get(position);
	}
}
