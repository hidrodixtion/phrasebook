package com.projectbox.phrasebook.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.projectbox.phrasebook.R;
import com.projectbox.phrasebook.data.SearchHelper;
import com.projectbox.phrasebook.data.model.Phrase;
import com.projectbox.phrasebook.event.FavClickEvent;
import com.projectbox.phrasebook.utility.SoundPlayer;

import de.greenrobot.event.EventBus;
import io.realm.RealmResults;

/**
 * Created by hidrodixtion on 10/7/15.
 */
public class SearchTranslationAdapter extends SingleListAdapter {
	public SearchTranslationAdapter(Context context, RealmResults<Phrase> data) {
		super(context, data);
	}

	@Override
	public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
		ViewHolderGroup vh;

		if (view == null) {
			view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_single_header, viewGroup, false);

			vh = new ViewHolderGroup(view);
			view.setTag(vh);
		}

		vh = (ViewHolderGroup) view.getTag();
		vh.txtItemHeader.setText(getGroup(i).getEnglish(), TextView.BufferType.SPANNABLE);

		return view;
	}

	@Override
	public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
		final ViewHolderContent vh;
		final Phrase phrase = getGroup(i);

		if (view == null) {
			view = LayoutInflater.from(mContext).inflate(R.layout.item_single_content, viewGroup, false);

			vh = new ViewHolderContent(view);
			view.setTag(vh);
		} else {
			vh = (ViewHolderContent) view.getTag();
		}

		String titleString = phrase.getTranslate();

		vh.txtItemContent.setText(phrase.getTranslate());

		if (!phrase.getRomaji().isEmpty()) {
			titleString = phrase.getRomaji();
			vh.txtRomaji.setText(phrase.getRomaji());
			vh.txtRomaji.setVisibility(View.VISIBLE);
		} else {
			vh.txtRomaji.setVisibility(View.GONE);
		}

		String searchString = SearchHelper.getInstance().getSearch();

		if (!searchString.isEmpty()) {
			SpannableString text = new SpannableString(titleString);
			int startIndex = titleString.toLowerCase().indexOf(searchString);

			if (startIndex >= 0 && startIndex < titleString.length() - 1)
				text.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), startIndex, startIndex + searchString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

			if (phrase.getRomaji().isEmpty()) {
				vh.txtItemContent.setText(text, TextView.BufferType.SPANNABLE);
			} else {
				vh.txtRomaji.setText(text, TextView.BufferType.SPANNABLE);
			}
		}


		vh.btnPlay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				SoundPlayer.getInstance().playSingle(mContext, phrase.getSound());
			}
		});

		final boolean isFavorite = phrase.isFavorite();

		if (isFavorite) {
			vh.btnFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_star));
		} else {
			vh.btnFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_star_border));
		}

		vh.btnFav.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!isFavorite) {
					mQHelper.markAsFavorite(phrase, true);
					vh.btnFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_star));
					EventBus.getDefault().post(new FavClickEvent(phrase.getEnglish(), true));
				} else {
					mQHelper.markAsFavorite(phrase, false);
					vh.btnFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_star_border));
					EventBus.getDefault().post(new FavClickEvent(phrase.getEnglish(), false));
				}
			}
		});
		return view;
	}

	@Override
	public void filter(String filterText) {
		filterText = filterText.toLowerCase();

		if (filterText.isEmpty())
			mPhrases = mOriginalPhrases;
		else
			mPhrases = mOriginalPhrases.where().beginGroup()
					.contains("translate", filterText, false)
					.or()
					.contains("romaji", filterText, false)
					.endGroup()
					.findAll();

		notifyDataSetChanged();
	}
}
