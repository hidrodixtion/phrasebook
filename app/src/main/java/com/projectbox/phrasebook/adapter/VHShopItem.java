package com.projectbox.phrasebook.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.projectbox.phrasebook.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by hidrodixtion on 8/20/15.
 */
public class VHShopItem extends RecyclerView.ViewHolder {
	@Bind(R.id.img_icon)
	ImageView imgIcon;
	@Bind(R.id.txt_title)
	TextView txtTitle;
	@Bind(R.id.txt_description)
	TextView txtDescription;
	@Bind(R.id.txt_buy)
	TextView txtBuy;
	@Bind(R.id.txt_price)
	TextView txtPrice;
	@Bind(R.id.img_bought)
	ImageView imgBought;
	@Bind(R.id.layout_buy)
	LinearLayout viewBuy;
	@Bind(R.id.parent_layout_buy)
	LinearLayout parentLayoutBuy;

	public VHShopItem(View itemView) {
		super(itemView);
		ButterKnife.bind(this, itemView);
	}
}
