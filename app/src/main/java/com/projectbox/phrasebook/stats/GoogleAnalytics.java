package com.projectbox.phrasebook.stats;

import android.app.Activity;

import com.projectbox.phrasebook.AnalyticsApplication;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;

/**
 * Created by ayublin on 11/2/15.
 */
public class GoogleAnalytics {

    private static GoogleAnalytics mInstance = null;

    private static Tracker mTracker;

    private GoogleAnalytics(){}

    private void setupTracker(Activity activity){
        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) activity.getApplication();
        mTracker = application.getDefaultTracker();

        if(mTracker != null) {
            // Enable Display Features.
            mTracker.enableAdvertisingIdCollection(true);
        }
    }

    public static GoogleAnalytics getInstance(Activity activity){
        if(mInstance == null)
        {
            mInstance = new GoogleAnalytics();
        }
        mInstance.setupTracker(activity);
        return mInstance;
    }

    public void sendScreenName(String name){
        if(mTracker == null)
            return;

        mTracker.setScreenName("Screen: " + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public void sendAction(String category, String action, String label, int value){
        if(mTracker == null)
            return;

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .setValue(value)
                .build());
    }

    public void trackPurchase(String pid, String tid, String pname, Double price, String appname) {
        if(mTracker == null)
            return;

        Product product =  new Product()
                .setId(pid)
                .setName(pname)
                .setCategory("All")
                .setVariant(appname)
                .setPrice(price)
                .setQuantity(1);
        ProductAction productAction = new ProductAction(ProductAction.ACTION_PURCHASE)
                .setTransactionId(tid)
                .setTransactionRevenue(price)
                .setTransactionTax(price * 0.2) // assume 20% tax
                .setTransactionShipping(0);
        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                .addProduct(product)
                .setProductAction(productAction);

        mTracker.setScreenName("Screen: ShopActivity");
        mTracker.send(builder.build());
    }

}
