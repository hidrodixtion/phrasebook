package com.projectbox.phrasebook;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ExpandableListView;

import com.projectbox.phrasebook.adapter.SingleListAdapter;
import com.projectbox.phrasebook.data.PreferenceHelper;
import com.projectbox.phrasebook.data.QueryHelper;
import com.projectbox.phrasebook.data.model.Phrase;
import com.projectbox.phrasebook.event.FilterEvent;
import com.projectbox.phrasebook.event.PlayEvent;
import com.projectbox.phrasebook.utility.SoundPlayer;
import com.software.shell.fab.ActionButton;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import io.realm.RealmResults;
import timber.log.Timber;


/**
 * A simple {@link Fragment} subclass.
 */
public class SingleListFragment extends Fragment {

	@Bind(R.id.list_view)
	ExpandableListView listView;
	@Bind(R.id.btn_play_all)
	ActionButton btnPlayAll;

	protected int _lastExpandedPost = -1;
	protected int _index;
	protected SingleListAdapter adapter;
	protected String mSearch;
	protected QueryHelper mQueryHelper;
	protected RealmResults<Phrase> mPhrases;
	protected String mTitle;

	public SingleListFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_single_list, container, false);
		ButterKnife.bind(this, view);

		Bundle bundle = getArguments();
		_index = bundle.getInt("position");
		mSearch = bundle.getString("search");
		mTitle = bundle.getString("title");

		Timber.v("title: " + mTitle);

		mQueryHelper = QueryHelper.getInstance();
		mPhrases = mQueryHelper.getContents().get(0).getSubcontents().get(_index).getPhrases().where().findAll();
		adapter = new SingleListAdapter(getActivity(), mPhrases);

		init();

		return view;
	}

	protected void init() {
		listView.setAdapter(adapter);
		listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

		listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
				if (_lastExpandedPost != i)
					listView.collapseGroup(_lastExpandedPost);

				if (PreferenceHelper.getInstance().isAutoPlay()) {
					listView.expandGroup(i);
					SoundPlayer.getInstance().playSingle(getActivity(), mPhrases.get(i).getSound());
				} else {
					if (listView.isGroupExpanded(i))
						listView.collapseGroup(i);
					else
						listView.expandGroup(i);
				}

				_lastExpandedPost = i;

				hideKeyboard();

				return true;
			}
		});

		adapter.filter(mSearch);

		if (mPhrases.size() == 0) {
			btnPlayAll.setVisibility(View.GONE);
		} else {
			btnPlayAll.setButtonColor(getResources().getColor(R.color.colorAccent));
			btnPlayAll.setButtonColorPressed(getResources().getColor(R.color.colorAccentDark));
			btnPlayAll.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_play_arrow));
			btnPlayAll.setShadowRadius(2.0f);
			btnPlayAll.setShadowYOffset(3.0f);
			btnPlayAll.setTag("paused");
		}

		if (_lastExpandedPost > -1) {
			Timber.v(_lastExpandedPost + "");
			listView.setSelectedGroup(_lastExpandedPost);
			listView.expandGroup(_lastExpandedPost);
		}
	}

	@OnClick(R.id.btn_play_all)
	public void onPlayAllClicked() {
		if (btnPlayAll.getTag().equals("paused")) {
			SoundPlayer.getInstance().playSequence(getActivity(), adapter.mPhrases, mTitle);
			btnPlayAll.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_stop));
			btnPlayAll.setTag("played");
		} else if (btnPlayAll.getTag().equals("played")) {
			SoundPlayer.getInstance().stopSequence();
			btnPlayAll.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_play_arrow));
			btnPlayAll.setTag("paused");
		}
	}

	protected void hideKeyboard() {
		View view = this.getActivity().getCurrentFocus();
		if (view != null) {
			InputMethodManager inputManager = (InputMethodManager) this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	protected void collapseAllItem() {
		listView.setSelectedGroup(-1);
		for (int i = 0; i < adapter.getGroupCount(); i++) {
			if (listView.isGroupExpanded(i)) {
				listView.collapseGroup(i);
			}
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.unbind(this);
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		EventBus.getDefault().unregister(this);
	}

	public void onEvent(FilterEvent e) {
		adapter.filter(e.filterText);
	}

	public void onEvent(PlayEvent e) {
		Timber.v("event: " + e.title);
		if (e.title.equals(SoundPlayer.SINGLE_PLAYING)) {
			if (btnPlayAll.getTag().equals("played")) {
				btnPlayAll.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_play_arrow));
				btnPlayAll.setTag("paused");
			}
		} else if (e.title.equals(mTitle)) {
			collapseAllItem();
			listView.expandGroup(e.index);
			_lastExpandedPost = e.index;
		}
	}
}
